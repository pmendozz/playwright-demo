pipeline {
    agent any

    tools {
        maven 'M2_HOME'
        jdk 'JAVA_HOME'
    }

    stages {
        stage('scm') {
            steps {
                echo 'Hello World'
            }
        }
        stage('test') {
            steps {
                sh 'mvn clean verify'
            }
        }
        stage('Generate HTML report') {
            steps {
                cucumber buildStatus: 'SUCCESS',
                        reportTitle: 'My report',
                        fileIncludePattern: 'target/build/*.json',
                        trendsLimit: 10,
                        classifications: [
                                [
                                        'key'  : 'Browser',
                                        'value': 'Firefox'
                                ]
                        ]
            }
        }
    }
}
