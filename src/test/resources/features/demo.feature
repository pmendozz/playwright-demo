@demo
Feature: Saucedemo Cart

  Background:
    Given el usuario se encuentra en la pagina de saucedemo
    When el usuario ingresa sus credenciales

  Scenario: Comprar de productos
    Given el usuario se encuentra en la pagina catálogo de saucedemo
    When el usuario selecciona un producto y valida que se agrego al carrito
    And el usuario valida el producto agregado y realiza el checkout de la compra ingresando la información "nombre", "apellido" y "código postal"
    Then el usuario visualiza el resumen de la compra y valida que el producto elejido sea el correcto
    When el usuario realiza el pago
    Then el usuario visualiza el mensaje de compra exitosa "Thank you for your order!"

  Scenario: Comprar de productos 2
    Given el usuario se encuentra en la pagina catálogo de saucedemo
    When el usuario selecciona un producto y valida que se agrego al carrito
    And el usuario valida el producto agregado y realiza el checkout de la compra ingresando la información "nombre", "apellido" y "código postal"
    Then el usuario visualiza el resumen de la compra y valida que el producto elejido sea el correcto
    When el usuario realiza el pago
    Then el usuario visualiza el mensaje de compra exitosa "Thank you for your order!"