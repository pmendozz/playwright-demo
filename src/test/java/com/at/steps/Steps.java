package com.at.steps;

import com.at.hook.Hook;
import com.at.page.PageHub;
import com.microsoft.playwright.Page;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class Steps {

    private final Page page = Hook.getPage();

    private PageHub pageHub() {
        return new PageHub();
    }

    @Given("el usuario se encuentra en la pagina de saucedemo")
    public void elUsuarioSeEncuentraEnLaPaginaDeSaucedemo() {
        Assert.assertTrue(page.title().contains("Swag Labs"));
    }

    @When("el usuario ingresa sus credenciales")
    public void elUsuarioIngresaSusCredenciales() {
        pageHub().loginPage(page).loginWithCredentials("standard_user", "secret_sauce");
    }

    @Given("el usuario se encuentra en la pagina catálogo de saucedemo")
    public void elUsuarioSeEncuentraEnLaPaginaCatálogoDeSaucedemo() {
        assertThat(pageHub().inventoryPage(page).getInventoryTitle()).containsText("Products");
    }

    @And("el usuario selecciona un producto y valida que se agrego al carrito")
    public void elUsuarioAgregaElProductoAlCarrito() {
        pageHub().inventoryPage(page).addProduct();
        assertThat(pageHub().inventoryPage(page).getInventoryCart()).containsText("1");
        pageHub().inventoryPage(page).goCart();
    }

    @And("el usuario valida el producto agregado y realiza el checkout de la compra ingresando la información {string}, {string} y {string}")
    public void elUsuarioRealizaElCheckoutDeLaCompraIngresandoLaInformaciónY(String nombre, String apellido, String code) {
        //cart page
        assertThat(pageHub().cartPage(page).getProductAdded()).containsText("Sauce Labs Backpack");
        pageHub().cartPage(page).checkout();
        //checkout page
        pageHub().checkoutPage(page).fillData(nombre, apellido, code);
        pageHub().checkoutPage(page).continueProcces();
    }

    @Then("el usuario visualiza el resumen de la compra y valida que el producto elejido sea el correcto")
    public void elUsuarioVisualizaElResumenDeLaCompraYValidaQueElProductoElejidoSeaElCorrecto() {
        //overview
        assertThat(pageHub().overviewPage(page).getProductOverview()).containsText("Sauce Labs Backpack");
    }

    @When("el usuario realiza el pago")
    public void elUsuarioRealizaElPago() {
        //overview
        pageHub().overviewPage(page).finish();
    }

    @Then("el usuario visualiza el mensaje de compra exitosa {string}")
    public void elUsuarioVisualizaElMensajeDeCompraExitosa(String message) {
        //lastpage
        assertThat(pageHub().lastPage(page).getFinalMessage()).containsText(message);
    }
}
