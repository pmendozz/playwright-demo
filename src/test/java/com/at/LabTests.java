package com.at;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"json:target/cucumber/cucumber.json",
                "html:target/cucumber/cucumber.html"},
        glue = {"com.at.steps", "com.at.hook"},
        features = "src/test/resources/features",
        tags = "@demo"
)
public class LabTests {
}
