package com.at.hook;

import com.microsoft.playwright.*;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class Hook {

    private static Page page;

    public static Page getPage() {
        return page;
    }

    public static void setPage(Page page) {
        Hook.page = page;
    }

    @Before
    public void init(){
        Playwright playwright = Playwright.create();
        Browser browser = playwright.chromium().launch(new BrowserType.LaunchOptions()
                .setHeadless(false));
        BrowserContext context = browser.newContext();
        page = context.newPage();
        page.navigate("https://www.saucedemo.com/");
        setPage(page);
    }

    @After
    public void whenFail(Scenario scenario){
        if(scenario.isFailed()){
            byte[] screenshot = getPage().screenshot();
            scenario.attach(screenshot, "image/jpeg", "evidencia");
        }
    }

}
