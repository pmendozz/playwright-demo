package com.at.page;

import com.microsoft.playwright.Page;

public class LoginPage {

    private Page page;
    public LoginPage(Page page) {
        this.page = page;
    }

    public void loginWithCredentials(String user, String pass){
        page.locator("[data-test=\"username\"]").fill(user);
        page.locator("[data-test=\"password\"]").fill(pass);
        page.locator("[data-test=\"login-button\"]").click();
    }

}
