package com.at.page;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class OverviewPage {

    Page page;
    public OverviewPage(Page page) {
        this.page = page;
    }

    public Locator getProductOverview() {
        return page.locator("#item_4_title_link");
    }

    public void finish() {
        page.locator("[data-test=\"finish\"]").click();
    }
}
