package com.at.page;

import com.microsoft.playwright.Page;

public class PageHub {

    public LoginPage loginPage(Page page){
        return new LoginPage(page);
    }

    public InventoryPage inventoryPage(Page page){
        return new InventoryPage(page);
    }

    public CartPage cartPage(Page page){
        return new CartPage(page);
    }

    public OverviewPage overviewPage(Page page){
        return new OverviewPage(page);
    }

    public CheckoutPage checkoutPage(Page page){
        return new CheckoutPage(page);
    }

    public LastPage lastPage(Page page){
        return new LastPage(page);
    }

}
