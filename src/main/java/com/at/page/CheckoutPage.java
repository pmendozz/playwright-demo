package com.at.page;

import com.microsoft.playwright.Page;

public class CheckoutPage {

    Page page;
    public CheckoutPage(Page page) {
        this.page = page;
    }

    public void fillData(String nombre, String apellido, String code) {
        page.locator("[data-test=\"firstName\"]").fill(nombre);
        page.locator("[data-test=\"lastName\"]").fill(apellido);
        page.locator("[data-test=\"postalCode\"]").fill(code);
    }

    public void continueProcces() {
        page.locator("[data-test=\"continue\"]").click();
    }
}
