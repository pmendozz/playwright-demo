package com.at.page;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;

public class LastPage {

    Page page;

    public LastPage(Page page) {
        this.page = page;
    }

    public Locator getFinalMessage() {
       return page.getByRole(AriaRole.HEADING);
    }
}
