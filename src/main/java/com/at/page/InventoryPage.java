package com.at.page;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class InventoryPage {

    Page page;
    public InventoryPage(Page page) {
        this.page = page;
    }

    public Locator getInventoryTitle() {
       return page.locator("#header_container");
    }

    public void addProduct(){
        page.locator("[data-test=\"add-to-cart-sauce-labs-backpack\"]").click();
    }

    public Locator getInventoryCart() {
        return page.locator("#shopping_cart_container");
    }

    public void goCart() {
        page.locator("a").filter(new Locator.FilterOptions().setHasText("1")).click();
    }
}
