package com.at.page;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class CartPage {

    Page page;
    public CartPage(Page page) {
        this.page = page;
    }


    public Locator getProductAdded() {
        return page.locator("#item_4_title_link");
    }

    public void checkout() {
        page.locator("[data-test=\"checkout\"]").click();
    }
}
